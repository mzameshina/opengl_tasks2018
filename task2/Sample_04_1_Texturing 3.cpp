#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Application.hpp>
#include <LightInfo.hpp>
#include <Texture.hpp>

#include <iostream>
#include <sstream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <chrono>



glm::vec3 createPoint(float v, float u, float aa=1)
{
    return glm::vec3((aa * aa / 2) * sin(2 * u) * cos(v) * cos(v), ( aa * aa / 2) * sin(u) * sin(2 * v), (aa * aa / 2) * cos(u) * sin(2 * v));
}


glm::vec3 normal(float v, float u, float eps=1e-6)
{
    auto point =createPoint(v, u);
    auto line1=(createPoint(v, u + eps) - point)/eps;
    auto line2=(createPoint(v + eps, u) - point)/eps;
    return glm::normalize(glm::cross(line1, line2));
}

std::vector<glm::vec3> vertices;
std::vector<glm::vec3> normals;

void vecAdd(int d, float min_u, float max_u, float min_v, float max_v, int param = 50, int less =0)
{
    for (int i = 0; i < d; ++i)
    {
        float u1 = min_u + (max_u - min_u) * i / d;
        bool st;
        if (less == 0)
        {
            st = (u1 < float(param)/100);
        }
        else
        {
            st = (u1 >= float(param)/100);
        }
        if (st)
        {
            float u2 = min_u + (max_u - min_u) * (i + 1) / d;
            for (int j = 0; j < d; ++j)
            {
                float v1 = min_v + (max_v - min_v) * j / d;
                float v2 = min_v + (max_v - min_v) * (j + 1) / d;

                vertices.push_back(createPoint(u1, v1));
                vertices.push_back(createPoint(u1, v2));
                vertices.push_back(createPoint(u2, v1));

                normals.push_back(createPoint(u1, v1));
                normals.push_back(createPoint(u1, v2));
                normals.push_back(createPoint(u2, v1));

                vertices.push_back(createPoint(u2, v1));
                vertices.push_back(createPoint(u1, v2));
                vertices.push_back(createPoint(u2, v2));

                normals.push_back(createPoint(u2, v1));
                normals.push_back(createPoint(u1, v2));
                normals.push_back(createPoint(u2, v2));
            }
        }
    }
}


MeshPtr createFig(int d, float min_u, float max_u, float min_v, float max_v, int param = 50, int less = 0)
{
    vecAdd(d, min_u, max_u, min_v, max_v, param, less);

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}


class SampleApplication : public Application
{

private:
    float _min_u = 0;
    float _max_u = glm::pi<float>() ;
    float _min_v = 0;
    float _max_v = 2 * glm::pi<float>() ;

    float _lr;
    float _phi;
    float _theta;

    LightInfo _light;
    TexturePtr _firstTexture;
    TexturePtr _secondTexture;
    GLuint _sampler;

public:
    MeshPtr _fig;
    MeshPtr _fig1;
    MeshPtr _marker;
    GLuint _cubeTexSampler;

    ShaderProgramPtr _shader;
    ShaderProgramPtr _marker_shader;
    ShaderProgramPtr _shader1;

    int param = 50;
    int _detalization = 100;

    void updateF(int &param)
    {
        if (param < 100 )
        {
            param += 1;
        }
        else
        {
            param = 0;
        }
        _fig = createFig(_detalization, _min_u, _max_u, _min_v, _max_v, float(param)/100, 0);
        _fig1 = createFig(_detalization, _min_u, _max_u, _min_v, _max_v, float(param)/100, 1);


    }


    void makeScene() override
    {

        Application::makeScene();
        //_cameraMover = std::make_shared<FreeCameraMover>();

        //Создаем меш из файла

        updateF(param);
        _fig->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));
        _fig1->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));
        _marker = makeSphere(0.1f);


        //Создаем шейдерную программу
        _shader = std::make_shared<ShaderProgram>("shaders4/texture.vert", "shaders4/texture.frag");
        _marker_shader = std::make_shared<ShaderProgram>("shaders/marker.vert", "shaders/marker.frag");
        _shader1 = std::make_shared<ShaderProgram>("shaders4/texture.vert", "shaders4/texture.frag");


        _lr = 10.0;
        _phi = 10.0;
        _theta = glm::pi<float>() * 0.25f;



        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * (float)_lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);


        glGenSamplers(1, &_cubeTexSampler);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

//Texture

        _firstTexture = loadTexture("images/grass.jpg");
        _secondTexture = loadTexture("images/earth_global.jpg");

        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }




    void draw() override
    {
        Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        //Устанавливаем общие юниформ-переменные

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        _shader->setMat4Uniform("modelMatrix", _fig->modelMatrix());

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * (float)_lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

        _shader->setVec3Uniform("light.pos", lightPosCamSpace);
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        _firstTexture->bind();
        //_secondTexture->bind();

        _shader->setIntUniform("diffuseTex", 0);

        {
            _shader->setMat4Uniform("modelMatrix", _fig->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _fig->modelMatrix()))));

            using namespace std::chrono;
            milliseconds ms = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
            float phase = (sin(ms.count() / 1000.0 + 1)) / 2;
            _shader->setFloatUniform("phase", phase);
            updateF(param);
            _fig->draw();
        }

        _shader->setMat4Uniform("modelMatrix", _fig->modelMatrix());


        //Подключаем шейдер
        _shader1->use();

        //Устанавливаем общие юниформ-переменные

        _shader1->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader1->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        _shader1->setMat4Uniform("modelMatrix", _fig1->modelMatrix());

        _shader1->setVec3Uniform("light.pos", lightPosCamSpace);
        _shader1->setVec3Uniform("light.La", _light.ambient);
        _shader1->setVec3Uniform("light.Ld", _light.diffuse);
        _shader1->setVec3Uniform("light.Ls", _light.specular);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        _secondTexture->bind();
        _shader1->setIntUniform("diffuseTex", 0);

        {
            _shader1->setMat4Uniform("modelMatrix", _fig1->modelMatrix());
            _shader1->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _fig1->modelMatrix()))));

            using namespace std::chrono;
            milliseconds ms = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
            float phase = (sin(ms.count() / 1000.0 + 1)) / 2;
            _shader1->setFloatUniform("phase", phase);
            updateF(param);
            _fig1->draw();
        }

        _shader1->setMat4Uniform("modelMatrix", _fig1->modelMatrix());
        {
            _marker_shader->use();

            _marker_shader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
            _marker_shader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
            _marker->draw();
        }

        glBindSampler(0, 0);
        glUseProgram(0);


    }




};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}