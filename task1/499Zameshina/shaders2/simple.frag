/*
Простейший фрагментный шейдер. Назначает фрагменту фиксированный цвет.
*/

#version 330

in vec4 color;
out vec4 fragColor;

void main()
{
	fragColor = color;
}
