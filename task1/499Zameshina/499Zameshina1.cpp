#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>

glm::vec3 createPoint(float v, float u, float aa=1)
{
    return glm::vec3((aa * aa / 2) * sin(2 * u) * cos(v) * cos(v), ( aa * aa / 2) * sin(u) * sin(2 * v), (aa * aa / 2) * cos(u) * sin(2 * v));
}


glm::vec3 normal(float v, float u, float eps=1e-6)
{
    auto point =createPoint(v, u);
    auto line1=(createPoint(v, u + eps) - point)/eps;
    auto line2=(createPoint(v + eps, u) - point)/eps;
    return glm::normalize(glm::cross(line1, line2));
}

std::vector<glm::vec3> vertices;
std::vector<glm::vec3> normals;

void vecAdd(int d, float min_u, float max_u, float min_v, float max_v)
{
    for (int i = 0; i < d; ++i)
    {
        float u1 = min_u + (max_u - min_u) * i / d;
        float u2 = min_u + (max_u - min_u) * (i + 1) / d;
        for (int j = 0; j < d; ++j)
        {
            float v1 = min_v + (max_v - min_v) * j / d;
            float v2 = min_v + (max_v - min_v) * (j + 1) / d;

            vertices.push_back(createPoint(u1, v1));
            vertices.push_back(createPoint(u1, v2));
            vertices.push_back(createPoint(u2, v1));

            normals.push_back(createPoint(u1, v1));
            normals.push_back(createPoint(u1, v2));
            normals.push_back(createPoint(u2, v1));

            vertices.push_back(createPoint(u2, v1));
            vertices.push_back(createPoint(u1, v2));
            vertices.push_back(createPoint(u2, v2));

            normals.push_back(createPoint(u2, v1));
            normals.push_back(createPoint(u1, v2));
            normals.push_back(createPoint(u2, v2));
        }
    }
}


MeshPtr createFig(int d, float min_u, float max_u, float min_v, float max_v)
{
    vecAdd(d, min_u, max_u, min_v, max_v);

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}


class SampleApplication : public Application
{
public:
    MeshPtr _fig;

    ShaderProgramPtr _shader;
    float _min_u = -10;
    float _max_u = 10;
    float _min_v = -10;
    float _max_v = 10;
    int _detalization = 100;

    void updateF()
    {
        _fig = createFig(_detalization, _min_u, _max_u, _min_v, _max_v);
    }


    void makeScene() override
    {
        Application::makeScene();

        updateF();

        _shader = std::make_shared<ShaderProgram>("shaders2/simple.vert", "shaders2/simple.frag");
    }

    void update() override
    {
        Application::update();
        updateF();
    }


    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);
            ImGui::Text("detalization %d", _detalization);
        }
        ImGui::End();
    }

    void handleKey(int key, int scancode, int action, int mods) override
    {
        Application::handleKey(key, scancode, action, mods);
        if (action == GLFW_PRESS)
        {
            if (key == GLFW_KEY_EQUAL)
            {
                _detalization += 1;
                updateF();
            }

            if (key == GLFW_KEY_MINUS)
            {
                _detalization -= 1;
                _detalization = std::max(_detalization, 1);
                updateF();
            }
        }
    }

    void draw() override
    {
        Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменные: время и матрицы
        _shader->setFloatUniform("time", (float)glfwGetTime()); //передаем время в шейдер

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        _shader->setMat4Uniform("modelMatrix", _fig->modelMatrix());
        _fig->draw();
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}